# TP3 : On va router des trucs

## I. ARP

### 1. Echange ARP

🌞**Générer des requêtes ARP**

- effectuer un `ping` d'une machine à l'autre

john
```
[ml@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.477 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=1.01 ms
```
marcel
```
[ml@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.421 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.706 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.351 ms
```
- observer les tables ARP des deux machines

john
````
[ml@localhost ~]$ arp -a
? (10.3.1.12) at 08:00:27:fb:07:8a [ether] on enp0s8
? (10.3.1.1) at 0a:00:27:00:00:03 [ether] on enp0s8
````
marcel
````
[ml@localhost ~]$ arp -a
? (10.3.1.11) at 08:00:27:4d:6b:73 [ether] on enp0s8
? (10.3.1.1) at 0a:00:27:00:00:03 [ether] on enp0s8
````
adresse MAC de john : 08:00:27:4d:6b:73
adresse MAC de marcel : 08:00:27:fb:07:8a

- prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)

john
````
[ml@localhost ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4d:6b:73 brd ff:ff:ff:ff:ff:ff
````
marcel
````
[ml@localhost ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:fb:07:8a brd ff:ff:ff:ff:ff:ff
````

on voit donc que les adresses MAC sont biens similaires 


### 2. Analyse de trames

🌞**Analyse de trames**

john
```
[ml@localhost ~]$ sudo ip n f all
[ml@localhost ~]$ sudo tcpdump -n arp -w tp2_arp.pcap
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), snapshot length 262144 bytes
^C4 packets captured
```
marcel
```
[ml@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.814 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.712 ms
```

🦈 **Capture réseau `tp3_arp.pcapng`** qui contient un ARP request et un ARP reply

[ARP](./pics/tp3_arp.pcapng)

## II. Routage

### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router`**

router
```
[ml@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[ml@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

🌞**Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**

john
```
[ml@localhost network-scripts]$ ip route show
10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.11 metric 100
10.3.2.0/24 via 10.3.1.254 dev enp0s8 proto static metric 100
[ml@localhost network-scripts]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.61 ms
```

marcel
```
[ml@localhost network-scripts]$ ip route show
10.3.1.0/24 via 10.3.2.254 dev enp0s8 proto static metric 100
10.3.2.0/24 dev enp0s8 proto kernel scope link src 10.3.2.12 metric 100
[ml@localhost network-scripts]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=63 time=0.836 ms
```

### 2. Analyse de trames

🌞**Analyse des échanges ARP**

- videz les tables ARP des trois noeuds
- effectuez un `ping` de `john` vers `marcel`
- regardez les tables ARP des trois noeuds
- essayez de déduire un peu les échanges ARP qui ont eu lieu
- répétez l'opération précédente (vider les tables, puis `ping`), en lançant `tcpdump` sur `marcel`
- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames** utiles pour l'échange


| ordre | type trame  | IP source | MAC source                 | IP destination | MAC destination            |
|-------|-------------|-----------|----------------------------|----------------|----------------------------|
| 1     | Requête ARP |10.3.1.11  | `john` `08:00:27:4d:6b:73` |  10.3.2.254    | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP |10.3.2.12  |`marcel` `08:00:27:fb:07:8a`|  10.3.1.11     | `john` `08:00:27:4d:6b:73` |
| 3     | Ping        |10.3.2.254 |`router` `08:00:27:e1:86:09`|  10.3.2.12     |`marcel` `08:00:27:fb:07:8a`|
| 3     | Pong        |10.3.2.12  |`marcel` `08:00:27:fb:07:8a`|  10.3.2.254    |`router` `08:00:27:e1:86:09`|
| 5     | Requête ARP |10.3.2.12  |`marcel` `08:00:27:fb:07:8a`|  10.3.2.254    |`router` `08:00:27:e1:86:09`|
| 6     | Réponse ARP |10.3.2.254 |`router` `08:00:27:e1:86:09`|  10.3.2.12     |`marcel` `08:00:27:fb:07:8a`|


🦈 **Capture réseau `tp3_routage_marcel.pcapng`**

[ARP](./pics/tp3_routage_marcel.pcap)
[ICMP](./pics/tp3_icmp.pcap)

### 3. Accès internet

🌞**Donnez un accès internet à vos machines**

- ajoutez une carte NAT en 3ème inteface sur le `router` pour qu'il ait un accès internet

  router 
  ```
  [ml@localhost ~]$ ping 8.8.8.8
  PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
  64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=23.2 ms
  ```
- ajoutez une route par défaut à `john` et `marcel`

  john
  ```
  [ml@localhost network-scripts]$ ping 8.8.8.8
  PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
  64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=26.3 ms
  ```
  marcel
  ```
  [ml@localhost network-scripts]$ ping 8.8.8.8
  PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
  64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=17.3 ms
  ```
- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser

  john
  ```
  [ml@localhost network-scripts]$ dig gitlab.com
  ; <<>> DiG 9.16.23-RH <<>> gitlab.com
  ;; global options: +cmd
  ;; Got answer:
  ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 59185
  ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

  ;; OPT PSEUDOSECTION:
  ; EDNS: version: 0, flags:; udp: 512
  ;; QUESTION SECTION:
  ;gitlab.com.			IN	A

  ;; ANSWER SECTION:
  gitlab.com.		300	IN	A	172.65.251.78

  ;; Query time: 55 msec
  ;; SERVER: 8.8.8.8#53(8.8.8.8)
  ;; WHEN: Tue Oct 04 22:34:17 CEST 2022
  ;; MSG SIZE  rcvd: 55
  ```
  marcel
  ```
  [ml@localhost network-scripts]$ dig gitlab.com

  ; <<>> DiG 9.16.23-RH <<>> gitlab.com
  ;; global options: +cmd
  ;; Got answer:
  ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 38432
  ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

  ;; OPT PSEUDOSECTION:
  ; EDNS: version: 0, flags:; udp: 512
  ;; QUESTION SECTION:
  ;gitlab.com.			IN	A

  ;; ANSWER SECTION:
  gitlab.com.		300	IN	A	172.65.251.78

  ;; Query time: 36 msec
  ;; SERVER: 8.8.8.8#53(8.8.8.8)
  ;; WHEN: Tue Oct 04 22:35:16 CEST 2022
  ;; MSG SIZE  rcvd: 55
  ```
  - puis avec un `ping` vers un nom de domaine

  john
  ```
  [ml@localhost network-scripts]$ ping 1.1.1.1
  PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
  64 bytes from 1.1.1.1: icmp_seq=1 ttl=61 time=23.7 ms
  64 bytes from 1.1.1.1: icmp_seq=2 ttl=61 time=22.5 ms
  ```
  marcel
  ```
  [ml@localhost network-scripts]$  ping 1.1.1.1
  PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
  64 bytes from 1.1.1.1: icmp_seq=1 ttl=61 time=23.4 ms
  64 bytes from 1.1.1.1: icmp_seq=2 ttl=61 time=30.1 ms
  ```

🌞**Analyse de trames**

- effectuez un `ping 8.8.8.8` depuis `john`
- capturez le ping depuis `john` avec `tcpdump`
- analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre | type trame | IP source          | MAC source                | IP destination    | MAC destination          |     |
|-------|------------|--------------------|---------------------------|-------------------|--------------------------|-----|
| 1     | ping       | `john` `10.3.1.11` | `john` `08:00:27:4d:6b:73`| `8.8.8.8`         | `08:00:27:12:21:9d`      |     |
| 2     | pong       | 8.8.8.8            | `08:00:27:12:21:9d`       | `john` `10.3.1.11`|`john` `08:00:27:4d:6b:73`| ... |

🦈 **Capture réseau `tp3_routage_internet.pcapng`**

[PING](./pics/tp3_routage_internet.pcap)

## III. DHCP

### 1. Mise en place du serveur DHCP

🌞**Sur la machine `john`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

- installation du serveur sur `john`

john
```
[ml@localhost ~]$ sudo dnf install dhcp-server
[ml@localhost ~]$ sudo nano /etc/dhcp/dhcpd.conf
[ml@localhost ~]$ sudo firewall-cmd --permanent --add-port=67/udp
sucess
[ml@localhost ~]$ sudo systemctl enable --now dhcpd
[ml@localhost ~]$ sudo systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
     Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; vendor preset: disabled)
```

- faites lui récupérer une IP en DHCP à l'aide de votre serveur

bob
```
[ml@localhost ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
[sudo] password for ml:
[ml@localhost ~]$ sudo systemctl restart NetworkManager
[ml@localhost ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8a:3b:43 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s8
```

🌞**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :

  john
  ```
  [ml@localhost ~]$ sudo cat /etc/dhcp/dhcpd.conf
    #
    # DHCP Server Configuration file.
    #   see /usr/share/doc/dhcp-server/dhcpd.conf.example
    #   see dhcpd.conf(5) man page
    #
    default-lease-time 900;
    max-lease-time 10800;
    ddns-update-style none;
    authoritative;
    subnet 10.3.1.0 netmask 255.255.255.0 {
      range 10.3.1.12 10.3.1.253
      option routers 10.3.1.254;
      option subnet-mask 255.255.255.0;
      option domain-name-servers 8.8.8.8;
    }
  ```

- récupérez de nouveau une IP en DHCP sur `bob` pour tester :
  - `bob` doit avoir une IP

    bob
    ```
    [ml@localhost ~]$ ip a
    2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        inet 10.3.1.12/24 brd 10.3.1.255 scope global dynamic enp0s8
    ```
    - vérifier qu'il peut `ping` sa passerelle

    bob
    ```
    [ml@localhost ~]$ ping 10.3.1.254
    PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
    64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=0.844 ms
    ```

  - il doit avoir une route par défaut

    bob
    ```
    [ml@localhost ~]$ ip r
    default via 10.0.2.2 dev enp0s3 proto dhcp src 10.0.2.15 metric 100
    10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
    10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.12 metric 101
    ``` 
    - vérifier que la route fonctionne avec un `ping` vers une IP

    bob
    ```
    [ml@localhost ~]$ ping 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=91.8 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=80.6 ms
    ```

  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms

    - vérifier avec la commande `dig` que ça fonctionne

    bob
    ```
    [ml@localhost ~]$ dig google.com
    ; <<>> DiG 9.16.23-RH <<>> google.com
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 9961
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 512
    ;; QUESTION SECTION:
    ;google.com.			IN	A

    ;; ANSWER SECTION:
    google.com.		300	IN	A	172.217.169.46

    ;; Query time: 84 msec
    ;; SERVER: 192.168.1.1#53(192.168.1.1)
    ;; WHEN: Sat Oct 08 16:13:48 CEST 2022
    ;; MSG SIZE  rcvd: 55
    ```
    - vérifier un `ping` vers un nom de domaine
    ```
    [ml@localhost ~]$ ping 1.1.1.1
    PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
    64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=96.3 ms
    64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=105 ms
    ```

### 2. Analyse de trames

🌞**Analyse de trames**

bob
```
[ml@localhost ~]$ sudo dhclient -r
Killed old client process
[ml@localhost ~]$ sudo dhclient
```
bob 
```
[ml@localhost ~]$ sudo tcpdump -i enp0s8 -pvn port 67 and port 68 -w tp3_dhcp.pcap
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), snapshot length 262144 bytes
7 packets captured
7 packets received by filter
0 packets dropped by kernel
```
local
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ scp ml@10.3.1.12:/home/ml/tp3_dhcp.pcap ./Documents/
tp3_dhcp.pcap                                 100% 2530     3.7MB/s   00:00
```

🦈 **Capture réseau `tp3_dhcp.pcapng`**

[PING](./pics/tp3_dhcp.pcap)
