# B2 Réseau 2022 - TP1

# TP1 - Mise en jambes

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

En utilisant la ligne de commande (CLI) de votre OS :

**🌞 Affichez les infos des cartes réseau de votre PC**

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ifconfig en0
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	ether 38:f9:d3:a7:6b:e9
	inet 10.33.18.101 netmask 0xfffffc00 broadcast 10.33.19.255
```

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ifconfig en7
en7: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	ether 00:e0:4c:68:23:2b
```
vu que j'ai un Mac, je n'ai pas d'adresse IP car je ne l'utilise pas 

**🌞 Affichez votre gateway**

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ route get default | grep gateway
    gateway: 10.33.19.254
```
  
### En graphique (GUI : Graphical User Interface)

En utilisant l'interface graphique de votre OS :  

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

```
adresse MAC :/System Preferences/Network/WI-Fi/Advanced../Wi-Fi/
adresse IP et gateway: /System Preferences/Network/WI-Fi/Advanced../TCP-IP
```

### Questions

- 🌞 à quoi sert la [gateway](../../cours/lexique.md#passerelle-ou-gateway) dans le réseau d'YNOV ?

    La gateway est le lien entre le réseau D'YNOV et le réseau d'Internet entre autre la LAN et la WAN.

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ifconfig en0
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	inet 10.33.1.251 netmask 0xfffffc00 broadcast 10.33.3.255
```

🌞 **Il est possible que vous perdiez l'accès internet.** Expliquez pourquoi.

Il y a le risque d'avoir la même IP que quelqu'un sur le réseau et vu que ça suit la logique du premier arrivé, premier servi il est possible de perdre l'accès internet

---


# II. Exploration locale en duo

## 3. Modification d'adresse IP

🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

- vérifiez à l'aide de commandes que vos changements ont pris effet
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ifconfig en7
en7: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=6467<RXCSUM,TXCSUM,VLAN_MTU,TSO4,TSO6,CHANNEL_IO,PARTIAL_CSUM,ZEROINVERT_CSUM>
	ether 48:65:ee:1f:18:56
	inet6 fe80::c0b:b7e3:78f0:e34e%en7 prefixlen 64 secured scopeid 0x17
	inet 192.168.1.1 netmask 0xfffffffc broadcast 192.168.1.3
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect (1000baseT <full-duplex>)
	status: active
```
- utilisez `ping` pour tester la connectivité entre les deux machines
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ping 192.168.1.2
PING 192.168.1.2 (192.168.1.2): 56 data bytes
64 bytes from 192.168.1.2: icmp_seq=0 ttl=64 time=1.933 ms
64 bytes from 192.168.1.2: icmp_seq=1 ttl=64 time=1.486 ms
```
- affichez et consultez votre table ARP
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ arp -a | grep 192.168.1.2
? (192.168.1.2) at 0:e0:4c:68:2b:94 on en7 ifscope [ethernet]
```

## 4. Utilisation d'un des deux comme gateway

- 🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu

  - encore une fois, un ping vers un DNS connu comme `1.1.1.1` ou `8.8.8.8` c'est parfait
  ```
  MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ping 1.1.1.1
  PING 1.1.1.1 (1.1.1.1): 56 data bytes
  64 bytes from 1.1.1.1: icmp_seq=0 ttl=247 time=49.553 ms
  64 bytes from 1.1.1.1: icmp_seq=1 ttl=247 time=31.941 ms
  64 bytes from 1.1.1.1: icmp_seq=2 ttl=247 time=46.313 ms
  ```

- 🌞 utiliser un `traceroute` ou `tracert` pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ traceroute 192.168.1.1
traceroute to 192.168.1.1 (192.168.1.1), 64 hops max, 52 byte packets
 1  192.168.1.1 (192.168.1.1)  2.266 ms  1.087 ms  0.753 ms
 ```
(j'ai changé de duo entre temps)

## 5. Petit chat privé

- 🌞 **sur le PC *serveur*** avec par exemple l'IP 192.168.1.1
```
C:\Users\Théo\Downloads\netcat-win32-1.11\netcat-1.11>nc -l -p 8888
```

- 🌞 **sur le PC *client*** avec par exemple l'IP 192.168.1.2
  ```
  MacBook-Pro-de-saperlipopette:~ marieliserenzema$ nc 192.168.1.1 8888
  salut
  yo !
  kjfgjsekl
  ```

---

- 🌞 pour aller un peu plus loin

  - le serveur peut préciser sur quelle IP écouter, et ne pas répondre sur les autres
  coté server 
  ```
  C:\WINDOWS\system32> netstat -a -n -b | select-string 9999
  TCP    0.0.0.0:9999           0.0.0.0:0              LISTENING
   
  C:\Users\Théo\Downloads\netcat-win32-1.11\netcat-1.11>nc -l -p 9999 -s 192.168.1.1

  C:\WINDOWS\system32> netstat -a -n -b | select-string 9999
  TCP    192.168.1.1:9999       0.0.0.0:0              LISTENING
  ```

## 6. Firewall

- activer le firewall 

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ % sudo pfctl -e
No ALTQ support in kernel
ALTQ related functions disabled
pf enabled
```

- 🌞 Autoriser les `ping`
- 🌞 Autoriser le traffic sur le port qu'utilise `nc`
  - on parle bien d'ouverture de **port** TCP et/ou UDP
  - on ne parle **PAS** d'autoriser le programme `nc`
  - choisissez arbitrairement un port entre 1024 et 20000
  - vous utiliserez ce port pour [communiquer avec `netcat`](#5-petit-chat-privé-) par groupe de 2 toujours
  - le firewall du *PC serveur* devra avoir un firewall activé et un `netcat` qui fonctionne

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ~ % sudo cat /etc/pf.conf
scrub-anchor "com.apple/*"
nat-anchor "com.apple/*"
rdr-anchor "com.apple/*"
dummynet-anchor "com.apple/*"
anchor "com.apple/*"
load anchor "com.apple" from "/etc/pf.anchors/com.apple"

set skip on lo0
# create a set skip rule for the loopback device
# block all connection (in/out)
block all

allow tcp for nc at 2000 port
pass in proto tcp to port { 2000 }
pass out proto tcp to port { 2000 }

# allow ping
pass out inet proto icmp icmp-type { echoreq }
pass in inet proto icmp icmp-type { echorep }
```

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ % sudo sudo pfctl -f /etc/pf.conf
```
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ~ % sudo reboot
```
```
(MacBook-Pro-de-saperlipopette:~ marieliserenzema$ /etc % sudo sudo pfctl -si
No ALTQ support in kernel
ALTQ related functions disabled
Status: Enabled for 0 days 00:08:58           Debug: Urgent

State Table                          Total             Rate
  current entries                        0               
  searches                            7369           13.7/s
  inserts                                0            0.0/s
  removals                               0            0.0/s
Counters
  match                               3631            6.7/s
  bad-offset                             0            0.0/s
  fragment                               0            0.0/s
  short                                  0            0.0/s
  normalize                              0            0.0/s
  memory                                 0            0.0/s
  bad-timestamp                          0            0.0/s
  congestion                             0            0.0/s
  ip-option                              0            0.0/s
  proto-cksum                            0            0.0/s
  state-mismatch                         0            0.0/s
  state-insert                           0            0.0/s
  state-limit                            0            0.0/s
  src-limit                              0            0.0/s
  synproxy                               0            0.0/s
  dummynet                               0            0.0/s
  invalid-port                           0            0.0/s
```

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ /etc % nc -l 2000                     
coucou
```
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ % nc 192.168.1.13 2000
coucou
```
  
# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

🌞Exploration du DHCP, depuis votre PC

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ipconfig getpacket en0
server_identifier (ip): 10.33.19.254
lease_time (uint32): 0x2864b
```

## 2. DNS

- 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ scutil --dns | grep nameserver
  nameserver[0] : 8.8.8.8
```

- 🌞 utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

 - faites un lookup (lookup = "dis moi à quelle IP se trouve tel nom de domaine")
  ```
  MacBook-Pro-de-saperlipopette:~ marieliserenzema$ nslookup google.com
  Name:	google.com
  Address: 216.58.213.78

  MacBook-Pro-de-saperlipopette:~ marieliserenzema$ nslookup ynov.com
  Non-authoritative answer:
  Name:	ynov.com
  Address: 104.26.10.233
  Name:	ynov.com
  Address: 172.67.74.226
  Name:	ynov.com
  Address: 104.26.11.233
  ```

  - déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes
  ```
  MacBook-Pro-de-saperlipopette:~ marieliserenzema$ nslookup ynov.com
  Server:		8.8.8.8
  ```

  - faites un *reverse lookup* (= "dis moi si tu connais un nom de domaine pour telle IP")
  ```
  MacBook-Pro-de-saperlipopette:~ marieliserenzema$ nslookup 78.74.21.21
  Non-authoritative answer:
  21.21.74.78.in-addr.arpa	name = host-78-74-21-21.homerun.telia.com.
  Authoritative answers can be found from:

  MacBook-Pro-de-saperlipopette:~ marieliserenzema$ nslookup 92.146.54.88
  ** server can't find 88.54.146.92.in-addr.arpa: NXDOMAIN
  ```


# IV. Wireshark

- 🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

  - un `ping` entre vous et la passerelle

  ![PING](./pics/pic_1.png)

  - un `netcat` entre vous et votre mate, branché en RJ45

  ![PING](./pics/pic_2.png)

  - une requête DNS
  
  ![PING](./pics/pic_3.png)


