# TP4 : TCP, UDP et services réseau

# I. First steps

Faites-vous un petit top 5 des applications que vous utilisez sur votre PC souvent, des applications qui utilisent le réseau : un site que vous visitez souvent, un jeu en ligne, Spotify, j'sais po moi, n'importe.

🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

`Messenger`

IP = 157.240.21.16 ; Port = 56284 ; Port Local = 443

[Messenger](./pics/messenger.pcapng)

`Discord`

IP = 162.159.133.234 ; Port = 63956 ; Port Local = 443

[Discord](./pics/discord.pcapng)

`Youtube`

IP = 173.194.0.135 ; Port = 64025 ; Port Local = 443

[Youtube](./pics/youtube.pcapng)

`Spotify`

IP = 151.101.122.248 ; Port = 63907 ; Port Local = 443

[Spotify](./pics/spotify.pcapng)

`Netflix`

IP = 198.38.120.173 ; Port = 56607 ; Port Local = 443

[Netflix](./pics/netflix.pcapng)


🌞 **Demandez l'avis à votre OS**

Messenger
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ netstat -n -b | grep 157.240.21.16
tcp4       0      0  192.168.68.102.56481   157.240.21.16.443      ESTABLISHED       6028      24658
```

Discord
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ netstat -n -b | grep 162.159.133.234
tcp4       0      0  192.168.68.102.63956   162.159.133.234.443    ESTABLISHED      47523       5214
```

Youtube
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ netstat -n -b | grep 173.194.0.135
udp4       0      0  192.168.68.102.64025   173.194.0.135.443                     2882199      27267
```

Spotify
````
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ netstat -n -b | grep 151.101.122.248
tcp4       0      0  192.168.68.102.64044   151.101.122.248.443    ESTABLISHED      39046       2764
````

Netflix
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ netstat -n -b | grep 198.38.120.173
tcp4       0      0  192.168.68.102.56609   198.38.120.173.443     ESTABLISHED   41886331     743882
```

🦈🦈🦈🦈🦈 **Bah ouais, captures Wireshark à l'appui évidemment.** Une capture pour chaque application, qui met bien en évidence le trafic en question.

# II. Mise en place

## 1. SSH


🌞 **Examinez le trafic dans Wireshark**

🌞 **Demandez aux OS**

- repérez, avec un commande adaptée, la connexion SSH depuis votre machine
```
[ml@localhost ~]$ tcpdump -i enp0s8 tcp port 22 -w dns.pcap
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), snapshot length 262144 bytes
96 packets captured
97 packets received by filter
0 packets dropped by kernel
```
[SSH](./pics/ssh.pcap)

local
```
mbpdesaipopette:~ marieliserenzema$ netstat -a -b
Active Internet connections (including servers)
Proto Recv-Q Send-Q  Local Address          Foreign Address        (state)        rxbytes    txbytes
tcp4       0      0  10.3.2.1.57373         10.3.2.12.ssh          ESTABLISHED    4139797    1248857
```

- ET repérez la connexion SSH depuis votre VM

john
```
[ml@localhost ~]$ ss | grep -i ssh
tcp   ESTAB 0      0                        10.3.2.12:ssh       10.3.2.1:57373
```

🦈 **Je veux une capture clean avec le 3-way handshake, un peu de trafic au milieu et une fin de connexion**

## 2. NFS

🌞 **Mettez en place un petit serveur NFS sur l'une des deux VMs**

server
```
[ml@localhost ~]$ cd /srv/shared/
[ml@localhost shared]$ ls
top-secret
```

client
```
[ml@localhost ~]$ sudo df -hT /mnt
Filesystem     Type  Size  Used Avail Use% Mounted on
10.3.1.11:/srv nfs4  6.2G  1.2G  5.1G  18% /mnt
[ml@localhost /]$ cd /mnt/shared/
[ml@localhost shared]$ ls
top-secret
```

🌞 **Wireshark it !**

- une fois que c'est en place, utilisez `tcpdump` pour capturer du trafic NFS
- déterminez le port utilisé par le serveur

🌞 **Demandez aux OS**

server
```
[ml@localhost shared]$ ss | grep nfs
tcp   ESTAB  0      0                        10.3.1.12:700          10.3.1.11:nfs
```

client
```
[ml@localhost ~]$ ss | grep nfs
tcp   ESTAB  0      0                        10.3.1.11:nfs          10.3.1.12:700
```

🦈 **Et vous me remettez une capture de trafic NFS** la plus complète possible. J'ai pas dit que je voulais le plus de trames possible, mais juste, ce qu'il faut pour avoir un max d'infos sur le trafic

## 3. DNS

🌞 Utilisez une commande pour effectuer une requête DNS depuis une des VMs

IP : 10.3.1.1
Port : 67

[DHCP](./pics/dhcp.pcap)
