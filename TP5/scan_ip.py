import ipaddress
import netifaces
import os
import scapy.all as scapy
from datetime import datetime
import platform


def check_ping(ip):
    if platform.system() == "Windows":
        command = "ping -n 1 {}"
    else:
        command = "ping -c 1 -W 1 {}"
    response = os.system(command.format(ip))
    if response == 0:
        return True
    else:
        return False


def create_file():
    if os.path.exists("scan_ip_report.txt"):
        os.remove("scan_ip_report.txt")
    file = open("scan_ip_report.txt", "a")
    file.write("------------------------------------------------------\n\t\t\t\tScan report\n------------------------------------------------------\n")
    return file


def display_result(start_time, end_time, f):
    duration = end_time - start_time
    f.write("\n\n------------------------------------------------------\n")
    f.write("[+] Start time: {}\n".format(start_time))
    f.write("[+] End time: {}\n".format(end_time))
    f.write("[+] Duration: {}\n".format(duration))


def main():
    index = netifaces.interfaces()
    print(index)
    target = input('Enter adresse network to scan : ')
    addrs = netifaces.ifaddresses(target)
    addr = addrs[netifaces.AF_INET][0]['addr']
    mask = addrs[netifaces.AF_INET][0]['netmask']
    iface = ipaddress.ip_interface(addr + "/" + mask)
    networkaddr = iface.network

    f = create_file()

    start_time = datetime.now()
    count_ip = 0

    for ip in networkaddr.hosts():
        if check_ping(ip):
            mac = scapy.getmacbyip(str(ip))
            if mac is not None:
                f.write("IP: {} \t==> IP Active \t MAC: {}\n".format(ip, mac))
            else:
                f.write("IP: {} \t==> IP Active \n".format(ip))
    end_time = datetime.now()
    display_result(start_time, end_time, f)
    f.close()


if __name__ == '__main__':
    main()
