# NETWORK SCANNER

> - scan_ip.py is a python script to find devices on your connected networks and see their ip
> - scan_port.py is a python script to find open ports from an ip


## How to use

- Clone and download the project and run ``cd TP5``
- To use scan_ip.py you have to download 
    - **Scapy:** with the command ``pip install scapy``
    - **Netifaces** with the command ``pip install netifaces``



## How to run 

- Run ``python scan_ip.py``
    1. Intput the name of network to scan
    - Exemple :
    ```
    ['lo', 'enp0s3', 'enp0s8']
    Enter adresse network to scan : enp0s8
    ```
    2. At the end run ``cat scan_ip_report.txt`` to read the scan report
    - Exemple:
    ```
    ------------------------------------------------------
				Scan report
    ------------------------------------------------------
    IP: 10.3.1.1 	==> IP Active 	 MAC: 0a:00:27:00:00:03
    IP: 10.3.1.11 	==> IP Active 	 MAC: ff:ff:ff:ff:ff:ff


    ------------------------------------------------------
    [+] Start time: 2022-10-30 13:28:53.925245
    [+] End time: 2022-10-30 13:33:07.227301
    [+] Duration: 0:04:13.302056
    ```

- Run ``python scan_port.py``
    1. Intput the ip to scan
    - Exemple :
    ```
    Enter adresse IP to scan : 10.3.1.11
    ```
    2. At the end run ``cat scan_port_report.txt`` to read the scan report
    - Exemple:
    ```
    ------------------------------------------------------
                    Scan report
    ------------------------------------------------------
    Port: 22 		 State: active
    Port: 111 		 State: active
    Port: 2049 		 State: active
    Port: 20048 		 State: active
    Port: 33609 		 State: active
    Port: 35121 		 State: active


    ------------------------------------------------------
    [+] Start time: 2022-10-30 13:39:41.237040
    [+] End time: 2022-10-30 13:39:46.094463
    [+] Duration: 0:00:04.857423
    ```


