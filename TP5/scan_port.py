from itertools import count
import os
import socket
from datetime import datetime
import concurrent.futures


def create_file():
    if os.path.exists("scan_port_report.txt"):
        os.remove("scan_port_report.txt")
    file = open("scan_port_report.txt", "a")
    file.write("------------------------------------------------------\n\t\t\t\tScan report\n------------------------------------------------------\n")
    return file


def display_result(start_time, end_time, f):
    duration = end_time - start_time
    f.write("\n\n------------------------------------------------------\n")
    f.write("[+] Start time: {}\n".format(start_time))
    f.write("[+] End time: {}\n".format(end_time))
    f.write("[+] Duration: {}\n".format(duration))


def scan_port(port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket.setdefaulttimeout(1)
    try:
        result = s.connect((str(ip), port))
        f.write("Port: {} \t\t State: {}\n".format(port, "active"))
        result.close()
    except:
        pass


def main():
    print('Démarage du scan de : ', ip)
    start_time = datetime.now()
    count_ports = 0
    ports = range(1, 65535)
    with concurrent.futures.ThreadPoolExecutor(max_workers=100) as executor:
        executor.map(scan_port, ports)

    end_time = datetime.now()
    print('Fin du scan de : ', ip)

    display_result(start_time, end_time, f)


if __name__ == '__main__':
    target = input('Enter adresse IP to scan : ')
    ip = socket.gethostbyname(target)
    f = create_file()
    main()
    f.close()
