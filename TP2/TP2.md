# TP2 : Ethernet, IP, et ARP

# I. Setup IP

🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**

  - les deux IPs choisies : 192.168.1.1/26 et 192.168.1.2/26
  - l'adresse de réseau : 192.168.1.0
  - l'adresse de broadcast : 192.168.1.63

    - vous renseignerez aussi les commandes utilisées pour définir les adresses IP *via* la ligne de commande
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ sudo ipconfig set en7 INFORM 192.168.1.2 255.255.255.192
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ifconfig en7
en7: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
    inet 192.168.1.2 netmask 0xffffffc0 broadcast 192.168.1.63
```

🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**

```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ping 192.168.1.1
PING 192.168.1.1 (192.168.1.1): 56 data bytes
64 bytes from 192.168.1.1: icmp_seq=0 ttl=64 time=1.154 ms
```


🌞 **Wireshark it**

- **déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par `ping`**

  - le ping que vous envoyez : 8 – Echo Request
  - le pong que vous recevez en retour : 0 – Echo Reply

🦈 **PCAP qui contient les paquets ICMP qui vous ont permis d'identifier les types ICMP**

[PING](./pics/ping.pcapng)

# II. ARP my bro


🌞 **Check the ARP table**

- utilisez une commande pour afficher votre table ARP
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ arp -a
```

- déterminez la MAC de votre binome depuis votre table ARP
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ arp 192.168.1.1
? (192.168.1.1) at 0:e0:4c:68:2b:94 on en7 ifscope [ethernet]
```

- déterminez la MAC de la *gateway* de votre réseau 
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ arp 10.33.19.254
? (10.33.19.254) at 0:c0:e7:e0:4:4e on en0 ifscope [ethernet]
```


🌞 **Manipuler la table ARP**

- utilisez une commande pour vider votre table ARP
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ sudo arp -a -d
10.33.16.4 (10.33.16.4) deleted
10.33.16.12 (10.33.16.12) deleted
10.33.16.66 (10.33.16.66) deleted
10.33.16.97 (10.33.16.97) deleted
10.33.16.156 (10.33.16.156) deleted
10.33.16.157 (10.33.16.157) deleted
10.33.16.159 (10.33.16.159) deleted
10.33.16.173 (10.33.16.173) deleted
10.33.16.191 (10.33.16.191) deleted
10.33.16.197 (10.33.16.197) deleted
```
- prouvez que ça fonctionne en l'affichant et en constatant les changements
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ arp -a
? (10.33.19.254) at 0:c0:e7:e0:4:4e on en0 ifscope [ethernet]
? (239.255.255.250) at 1:0:5e:7f:ff:fa on en0 ifscope permanent [ethernet]
```
- ré-effectuez des pings, et constatez la ré-apparition des données dans la table ARP
```
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ping 192.168.1.1
PING 192.168.1.1 (192.168.1.1): 56 data bytes
64 bytes from 192.168.1.1: icmp_seq=0 ttl=64 time=1.903 ms
64 bytes from 192.168.1.1: icmp_seq=1 ttl=64 time=1.623 ms
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ arp -a
? (10.33.16.4) at 82:54:43:12:29:71 on en0 ifscope [ethernet]
? (10.33.16.54) at c8:58:c0:27:22:fc on en0 ifscope [ethernet]
? (10.33.16.191) at a:f7:32:e1:8b:26 on en0 ifscope [ethernet]
? (10.33.16.224) at 96:1d:a:51:d3:c on en0 ifscope [ethernet]
? (10.33.16.229) at ec:63:d7:b6:df:8c on en0 ifscope [ethernet]
? (10.33.18.93) at 4e:df:32:7c:a2:47 on en0 ifscope [ethernet]
? (10.33.18.121) at fa:e7:6c:b:20:e3 on en0 ifscope [ethernet]
? (10.33.18.132) at f6:7a:23:89:cc:8e on en0 ifscope [ethernet]
? (10.33.18.184) at f0:18:98:77:9c:32 on en0 ifscope [ethernet]
? (10.33.19.71) at 52:76:de:b:6b:e2 on en0 ifscope [ethernet]
? (10.33.19.126) at 56:f5:b2:ca:92:9a on en0 ifscope [ethernet]
? (10.33.19.230) at a4:c3:f0:f1:f1:44 on en0 ifscope [ethernet]
? (10.33.19.232) at 3c:6:30:20:e4:20 on en0 ifscope [ethernet]
? (10.33.19.254) at 0:c0:e7:e0:4:4e on en0 ifscope [ethernet]
? (192.168.1.1) at 0:e0:4c:68:2b:94 on en7 ifscope [ethernet]
? (239.255.255.250) at 1:0:5e:7f:ff:fa on en0 ifscope permanent [ethernet]
```

🌞 **Wireshark it**

🦈 **PCAP qui contient les trames ARP**

[ARP](./pics/arp.pcapng)

 - ARP broadcast : adresse src -> 00:e0:4c:68:23:2b (moi)
                   adresse dst -> ff:ff:ff:ff:ff:ff (tout le monde)
 - ARP reply : adresse src ->  0:e0:4c:68:2b:94 (alexandre)
               adresse dst -> 00:e0:4c:68:23:2b (moi)

# III. DHCP you too my brooo

🌞 **Wireshark it**

🦈 **PCAP qui contient l'échange DORA**

[DHCP](./pics/dhcp.pcapng)

- **1.** l'IP à utiliser : 10.33.18.101
- **2.** l'adresse IP de la passerelle du réseau : 10.33.19.254
- **3.** l'adresse d'un serveur DNS joignable depuis ce réseau : 8.8.8.8


# IV. Avant-goût TCP et UDP

🌞 **Wireshark it**
  - il sera sûrement plus simple de repérer le trafic Youtube en fermant tous les autres onglets et toutes les autres applications utilisant du réseau

🦈 **PCAP qui contient un extrait de l'échange qui vous a permis d'identifier les infos**

[YTB](./pics/ytb.pcapng)

- déterminez à quelle IP et quel port votre PC se connecte quand vous regardez une vidéo Youtube
- ip : 172.217.22.138
- port : 50374